import wizText from 'wiz-text';
import wizImage from 'wiz-image';
import wizButton from 'wiz-button';
import { wizList } from 'wiz-list';
import wizContainer from 'wiz-container';
import wizItCoreHeader from '../common/components/wizItcoreHeader/index.vue';
import wizItBorderContainer from '../common/components/wizItCoreBorderContainer/index.vue';
import wizItChapterMenu from '../common/components/wizItChapterMenu/index.vue';
import wizItSidebar from '../common/components/wizItSidebar/index.vue';
import wizItSitemapMenu from '../common/components/wizItSitemapMenu/index.vue';

export default (Vue) => {
  Vue.component('wiz-text', wizText);
  Vue.component('wiz-image', wizImage);
  Vue.component('wiz-button', wizButton);
  Vue.component('wiz-container', wizContainer);
  Vue.component('wiz-it-core-header', wizItCoreHeader);
  Vue.component('wiz-list', wizList);
  Vue.component('wiz-it-border-container', wizItBorderContainer);
  Vue.component('wiz-it-chapter-menu', wizItChapterMenu);
  Vue.component('wiz-it-sidebar', wizItSidebar);
  Vue.component('wiz-it-sitemap-menu', wizItSitemapMenu);
};
